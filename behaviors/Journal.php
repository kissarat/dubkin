<?php

namespace app\behaviors;

use app\helpers\SQL;
use Exception;
use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * @property ActiveRecord $owner
 */
class Journal extends Behavior {

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'writeEvent',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'writeEvent',
            ActiveRecord::EVENT_BEFORE_DELETE => 'writeEvent',
        ];
    }

    public static function writeEvent(Event $event) {
        switch($event->name) {
            case ActiveRecord::EVENT_AFTER_INSERT:
                $name = 'create';
                break;
            case ActiveRecord::EVENT_BEFORE_UPDATE:
                $name = 'update';
                break;
            case ActiveRecord::EVENT_BEFORE_DELETE:
                $name = 'delete';
                break;
            default:
                $name = $event->name;
                break;
        }

        $model = $event->sender;
        $data = $event->data;
        if (('create' == $name || 'update' == $name) && method_exists($model, 'traceable')) {
            if (is_null($data)) {
                $data = [];
            }
            elseif (!is_array($data)) {
                throw new InvalidParamException(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
            }
            foreach($model->traceable() as $attribute) {
                if ('create' == $name && $model->$attribute || $model->isAttributeChanged($attribute, false)) {
                    $data[$attribute] = $model->$attribute;
                }
            }
        }

        $table = $event->sender->tableName();
        if (0 === strpos($table, '{{%')) {
            $table = substr($table, 3, -2);
        }
        static::write($table, $name, $model->id, $data);
    }

    public static function writeException($type, $event, $object_id = null, Exception $exception) {
        static::write($type, $event, $object_id, $exception);
    }

    public static function write($type, $event, $object_id = null, $data = null) {
        SQL::execute('INSERT INTO journal(type, event, object_id, user_id, ip, data)
            VALUES (:type, :event, :object_id, :user_id, :ip, :data)', [
            ':type' => $type,
            ':event' => $event,
            ':object_id' => $object_id,
            ':user_id' => Yii::$app->user->getIsGuest() ? null : Yii::$app->user->id,
            ':ip' => $_SERVER['REMOTE_ADDR'],
            ':data' => empty($data) ? null : json_encode($data),
        ]);
    }
}
