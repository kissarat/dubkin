<?php
use yii\widgets\DetailView;

/** @var \app\models\Invoice $model */

$url = ['index'];
if (!Yii::$app->user->can('manage')) {
    $url['user_id'] = Yii::$app->user->id;
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invoices'), 'url' => $url];
?>
<div class="invoice-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'amount',
            'wallet',
            'user_id',
            'status',
        ]
    ]) ?>
</div>
