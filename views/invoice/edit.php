<?php

use app\models\Invoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $form yii\widgets\ActiveForm */
$statuses = [];
foreach(Invoice::$statuses as $key => $value) {
    $statuses[$key] = Yii::t('app', $value);
}
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin();

    if ('manage' == $model->scenario) {
        echo $form->field($model, 'user_id')->textInput(['maxlength' => true]);
        echo $form->field($model, 'status')->dropDownList($statuses);
    }
    echo Html::activeHiddenInput($model, 'withdraw');
    echo $form->field($model, 'type')->dropDownList(['perfect' => 'Perfect Money']);
    echo $form->field($model, 'amount')->textInput(['maxlength' => true]);
    ?>

    <p>
        <?= Html::submitButton(Yii::t('app', $model->withdraw ? 'Withdraw' : 'Payment')) ?>
    </p>

    <?php ActiveForm::end(); ?>
</div>
