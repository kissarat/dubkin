<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
use yii\grid\GridView;
use yii\helpers\Html;

?>
<div class="invoice-index">
    <p>
        <?= Html::a(Yii::t('app', 'Payment'), ['payment']) ?>
        <?= Html::a(Yii::t('app', 'Withdraw'), ['withdraw']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider
    ]) ?>
</div>
