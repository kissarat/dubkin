<?php
/* @var $this yii\web\View */
/* @var $model \app\models\User */

use app\helpers\Country;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;

function spaces() {
    return implode(' ', func_get_args());
}

if (Yii::$app->user->can('manage')) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
}
?>
<div class="user-view">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Password'), ['password', 'id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Wallets'), ['wallet', 'id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Invoices'), ['invoice/index', 'user_id' => $model->id]) ?>
        <?= Html::a(Yii::t('app', 'Programs'), ['matrix/node/index', 'user_id' => $model->id]) ?>
    </p>

    <div class="attributes">
        <?= Html::tag('h2', $model->id) ?>
        <?= Html::tag('h3', spaces($model->forename, $model->surname, $model->patronymic)) ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'account',
                    'value' => (float) $model->account
                ],
                [
                    'attribute' => 'referral_id',
                    'format' => 'html',
                    'value' => Html::a($model->referral_id,
                        ['view', 'referral_id' => $model->referral_id],
                        ['class' => 'value'])
                ],
                'email',
                'skype',
                'phone',
                [
                    'attribute' => 'country',
                    'value' => $model->country ? Country::decode($model->country) : null
                ],
                'postal',
                'city',
                'address',
                [
                    'label' => Yii::t('app', 'Confidant'),
                    'value' => spaces($model->confidant_forename, $model->confidant_surname, $model->confidant_patronymic)
                ],
            ]
        ]) ?>
    </div>
</div>
