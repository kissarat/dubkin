<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/** @var \app\models\PasswordForm $model */
?>
<div class="user-reset">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'repeat')->passwordInput() ?>
    <?= Html::submitButton(Yii::t('app', 'Change')) ?>
    <?php ActiveForm::end() ?>
</div>
