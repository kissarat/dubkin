<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/** @var \app\models\RequestForm $model */
?>
<div class="user-request">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id') ?>
    <?= $form->field($model, 'email') ?>
    <?= Html::submitButton(Yii::t('app', 'Send Password')) ?>
    <?php ActiveForm::end() ?>
</div>
