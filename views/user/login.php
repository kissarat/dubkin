<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Login');
?>
<div class="user-login">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <p>
        <?= Html::submitButton($this->title) ?>
    <p>
    <?php ActiveForm::end(); ?>
</div><!-- user-login -->
