<?php
/* @var $this yii\web\View */
/* @var $model \app\models\User */

use app\helpers\Country;
use app\models\User;
use app\widgets\Fake;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if ($model->hasErrors()) {
    echo Html::script('var errors = ' . json_encode($model->getErrors()));
}

?>
<div class="user-edit">
    <?php
    $form = ActiveForm::begin([
        'options' => ['name' => 'User'],
        'fieldConfig' => function(User $model, $attribute) {
            return [
                'template' => '{input}{error}',
                'inputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => $model->attributeLabels()[$attribute]
                ]
            ];
        }
    ]);
    ?>

    <p>
        <?php if (Yii::$app->user->can('manage')): ?>
            <?= Fake::widget() ?>
        <?php endif ?>
    </p>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'referral_id') ?>
            <?= $form->field($model, 'id') ?>
            <?= $form->field($model, 'surname') ?>
            <?= $form->field($model, 'forename') ?>
            <?= $form->field($model, 'patronymic') ?>
            <?= $form->field($model, 'skype') ?>
            <?= $form->field($model, 'phone') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'country')->dropDownList(Country::getCountries()) ?>
            <?= $form->field($model, 'city') ?>
            <?= $form->field($model, 'postal') ?>
            <?= $form->field($model, 'address') ?>
            <?= $form->field($model, 'pin') ?>
            <?= $form->field($model, 'email') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'repeat')->passwordInput() ?>
            <?= $form->field($model, 'confidant_type')->dropDownList(User::getConfidantTypes()) ?>
            <?= $form->field($model, 'confidant_surname') ?>
            <?= $form->field($model, 'confidant_forename') ?>
            <?= $form->field($model, 'confidant_patronymic') ?>
        </div>
    </div>

    <p>
        <input type="checkbox" id="agree" />
        <label for="agree">я согласен с условиями компании</label>
    </p>

    <p>
        <?= Html::submitButton($this->title, ['class' => 'btn btn-primary']) ?>
    </p>

    <?php ActiveForm::end(); ?>
</div>
