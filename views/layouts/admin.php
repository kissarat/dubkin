<?php
use app\models\Config;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

$login = Yii::$app->user->getIsGuest() ? '' : 'login';
$route = [Yii::$app->controller->id, Yii::$app->controller->action->id];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"><!--

Домен зарегистрирован на имя разработчика и хоститься на его (разработчика) серверах,
но разработчик не являеться владельцем сайта и не несет ответственности за его действия,
т.е. разработчик не несет ответственности за действия владельца сайта,
которые, в том числе, включают в себя проведения любых денежных операций
связанных с кошельком Perfect Money <?= Config::get('perfect', 'wallet') .
"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ?>
-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/images/cover.png" />
    <?php $this->head() ?>
    <?= Html::csrfMetaTags() ?>
</head>
<body class="<?= implode(' ', $route) ?>">
<div id="background"></div>
<?php $this->beginBody();
if (!Yii::$app->user->getIsGuest()) {
    $user = Yii::$app->user->id;
    echo Html::script("var user = '$user'");
}
?>
<div class="wrap <?= $login ?>">
    <?php
    NavBar::begin();

    $items = [
        ['label' => Yii::t('app', 'User'), 'url' => ['/user/index']],
    ];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>
    <div style="text-align: center">
        <?= Html::a('', ['/page/marketing'], ['class' => 'start']) ?>
    </div>
    <?= Breadcrumbs::widget([
        'homeLink' => false,
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?php
    if ($description) {
        echo "<div class=\"alert alert-danger\">$description</div>";
    }
    ?>
    <div class="background">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>

<?php if (Config::get('common', 'heart')): ?>
    <script>
        (function(){
            var widget_id = <?= Config::get('common', 'heart') ?>;
            _shcp =[{widget_id : widget_id}];
            var lang =(navigator.language || navigator.systemLanguage
            || navigator.userLanguage ||"en")
                .substr(0,2).toLowerCase();
            var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
            var hcc = document.createElement("script");
            hcc.type ="text/javascript";
            hcc.async =true;
            hcc.src =("https:"== document.location.protocol ?"https":"http")
                +"://"+ url;
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hcc, s.nextSibling);
        })();
    </script>
<?php endif ?>

<?php if (Config::get('common', 'google')): ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?= Config::get('common', 'google') ?>', 'auto');
        ga('require', 'linkid', 'linkid.js');
        ga('send', 'pageview');
    </script>
<?php endif ?>
</body>
</html>
<?php $this->endPage() ?>
