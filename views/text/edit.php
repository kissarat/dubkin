<?php
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Update');
?>
<div class="text-edit">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id') ?>
    <?= Html::dropDownList('language', ['ru' => 'Русский', 'en' => 'English'], $model->lang, ['style' => '']) ?>
    <?= $form->field($model, 'content')->widget(CKEditor::class, ['preset' => 'full']) ?>

    <?= Html::submitButton($this->title) ?>

    <?php ActiveForm::end(); ?>
</div>
