<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\models\SettingsForm $model */
?>
<div class="setting-index">
    <?php
    $form = ActiveForm::begin();
    foreach($model->attributes() as $name) {
        echo $form->field($model, $name);
    }

    echo Html::submitButton(Yii::t('app', 'Save'));
    ActiveForm::end();
    ?>
</div>
