<?php
use app\models\Article;
use yii\helpers\Html;

?>
<div class="home-index">
    <div class="signup">
        <?= Html::a(Yii::t('app', 'Signup'), ['/user/signup']) ?>
    </div>
    <?= Article::widget('about') ?>
    <?= Article::widget('features') ?>
</div>
