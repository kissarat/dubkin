<?php
use app\models\Article;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Update');
?>
<div class="article-edit">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id') ?>
    <div class="form-group field-language">
        <label><?= Yii::t('app', 'Language') ?>:</label>
        <?php
        $languages = [];
        foreach(Article::languages() as $code => $name) {
            $languages[] = $model->lang == $code ? $name : Html::a($name, ['edit', 'id' => $model->id, 'lang' => $code]);
        }
        echo implode(' ', $languages);
        ?>
    </div>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'keywords') ?>
    <?= $form->field($model, 'summary')->textarea() ?>
    <?= $form->field($model, 'content')->widget(CKEditor::class, ['preset' => 'full']) ?>

    <?= Html::submitButton($this->title) ?>

    <?php ActiveForm::end(); ?>
</div>
