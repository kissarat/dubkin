<?php
use yii\grid\GridView;
/** @var \yii\data\ActiveDataProvider $dataProvider */

$url = ['/matrix/node/index'];
if (!Yii::$app->user->can('manage')) {
    $url['user_id'] = Yii::$app->user->id;
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Programs'), 'url' => $url];
?>
<div class="transfer-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'time:datetime',
            'user_id',
            'amount:integer'
        ]
    ]) ?>
</div>
