<?php

namespace app\models;


use Yii;
use yii\base\Model;

/**
 * @property string id
 * @property string password
 */
class RequestForm extends Model
{
    public $id;
    public $email;

    public function rules() {
        return [
            ['id', 'string'],
            ['email', 'email'],
            ['id', 'exist',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'User does not exist')],
            ['email', 'exist',
                'targetClass' => 'app\models\User',
                'message' => Yii::t('app', 'User does not exist')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Login'),
            'email' => 'Email'
        ];
    }

    /**
     * @return User
     */
    public function getUser() {
        $user = null;
        if ($this->id) {
            $user = User::findOne(['id' => $this->id]);
        }
        if ($this->email) {
            $user = User::findOne(['email' => $this->email]);
        }
        return $user;
    }
}
