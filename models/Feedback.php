<?php
/**
 * @link http://zenothing.com/
*/

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $email
 * @property string $subject
 * @property string $ticket
 * @property string $content
 * @property boolean $open
 * @author Taras Labiak <kissarat@gmail.com>
 */
class Feedback extends ActiveRecord {
    public $content;

    public static function tableName() {
        return 'feedback';
    }

    public function rules() {
        return [
            ['ticket', 'string', 'length' => 24],
            ['email', 'email'],
            [['subject'], 'required'],
            [['subject', 'content'], 'string'],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['subject', 'email', 'content', 'ticket', 'user_id'],
            'guest'   => ['subject', 'email', 'content', 'ticket']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'email' => Yii::t('app', 'Email'),
            'subject' => Yii::t('app', 'Subject'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    public function __toString() {
        return $this->subject;
    }

    public function getMessages() {
        return $this->hasMany(Message::class, ['feedback_id' => 'id']);
    }

    /**
     * @return Message
     */
    public function getFirst() {
        return Message::find()
            ->where(['feedback_id' => $this->id])
            ->orderBy(['time' => SORT_ASC])
            ->limit(1)
            ->one();
    }

    public function __debuginfo() {
        return json_encode([
            'attributes' => $this->getAttributes(),
            'errors' => $this->getErrors()
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
}
