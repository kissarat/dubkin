<?php

namespace app\models;


use Yii;
use yii\helpers\Html;

/**
 * @property string title
 * @property string keywords
 * @property string summary
 */
class Article extends Text
{
    public static function tableName()
    {
        return 'article';
    }

    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id', 'type'], 'string'],
            ['title', 'string', 'min' => 3, 'max' => 255],
            ['keywords', 'string', 'min' => 3, 'max' => 192],
            ['summary', 'string', 'min' => 3, 'max' => 192],
            ['content', 'string', 'min' => 20],
            [['type', 'keywords', 'summary', 'content'], 'default', 'value' => null]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang' => Yii::t('app', 'Language'),
            'title' => Yii::t('app', 'Title'),
            'keywords' => Yii::t('app', 'Keywords'),
            'summary' => Yii::t('app', 'Summary'),
            'content' => Yii::t('app', 'Content'),
        ];
    }

    public function getMetaTags() {
        $tags = [];
        if ($this->keywords) {
            $tags['keywords'] = $this->keywords;
        }
        if ($this->summary) {
            $tags['description'] = $this->summary;
        }
        return $tags;
    }

    public function __toString()
    {
        return $this->title;
    }

    public static function widget($condition) {
        if (is_string($condition)) {
            $condition = [
                'id' => $condition,
                'lang' => Yii::$app->language
            ];
        }

        $article = Article::findOne($condition);
        if (!$article) {
            $new = new Article($condition);
            if ($new->loadFile()) {
                $article = $new;
            }
        }
        return Yii::$app->view->render('@app/views/article/view', [
            'model' => $article,
            'condition' => $condition,
            'meta' => false
        ]);
    }
}
