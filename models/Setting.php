<?php

namespace app\models;
use app\helpers\SQL;
use Yii;

/**
 * @property string name
 * @property string value
 * @property boolean visible
 * @property string user_id
 */
class Setting extends UserRecord
{
    public function rules()
    {
        return [
            [['category', 'name'], 'required'],
            [['category', 'name', 'value'], 'string'],
            ['visible', 'boolean'],
            ['user_id', 'string'],
        ];
    }

    public function get($category, $name, $user_id = null) {
        if (empty($user_id)) {
            $user_id = Yii::$app->user->id;
        }
        return SQL::queryCell('SELECT "value" FROM setting WHERE user_id = :user_id
          AND "category" = :category AND "name" = :name', [
            ':user_id' => $user_id,
            ':category' => $category,
            ':name' => $name,
        ]);
    }
}
