<?php

namespace app\models;
use Yii;

/**
 * @property integer $id
 * @property string $type
 * @property string $event
 * @property string $user_id
 * @property string $object_id
 * @property string $ip
 * @property string $time
 */
class Transfer extends UserRecord
{
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' =>  Yii::t('app', 'Investor'),
            'time' => Yii::t('app', 'Time'),
        ];
    }
}
