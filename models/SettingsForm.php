<?php

namespace app\models;


use yii\base\Model;

/**
 * @property Setting[] $settings
 */
class SettingsForm extends Model
{
    public $settings;

    public function loadCategory($category, $user_id = null) {
        $this->settings = Setting::find()
                ->andWhere(['category' => $category])
                ->andWhere($user_id ? ['user_id' => $user_id] : 'user_id is null')
                ->andWhere('visible')
                ->orderBy('id')
                ->indexBy('name')
                ->all();
    }

    public function loadAll($user_id = null) {
        $query = Setting::find()
                ->andWhere($user_id ? ['user_id' => $user_id] : 'user_id is null')
                ->andWhere('visible')
                ->orderBy('id');
        $this->settings = [];
        foreach($query->each() as $setting) {
            $this->settings[$setting->category . '_' . $setting->name] = $setting;
        }
    }

    public function attributes()
    {
        return array_keys($this->settings);
    }

    public function rules()
    {
        $rules = [];
        foreach($this->attributes() as $name) {
            $rules[] = [$name, 'string'];
        }
        return $rules;
    }

    public function __get($name)
    {
        return $this->settings[$name]->value;
    }

    public function __set($name, $value)
    {
        $setting = $this->settings[$name];
        if ($setting->value != $value) {
            $setting->value = $value;
        }
    }

    public function save() {
        foreach($this->settings as $setting) {
            if ($setting->isAttributeChanged('value')) {
                if (!$setting->save()) {
                    return false;
                }
            }
        }
        return true;
    }
}
