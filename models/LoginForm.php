<?php

namespace app\models;


use Yii;
use yii\base\Model;


/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class LoginForm extends Model {
    public $name;
    public $password;

    public function rules() {
        return [
            [['name', 'password'], 'required'],
            ['name', 'string', 'min' => 4, 'max' => 24],
            ['password', 'string', 'min' => 1],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    public static function login(User $user) {
        Yii::$app->user->login($user);
    }

    /**
     * @return User
     */
    public function getUser() {
        return User::findOne(['id' => $this->name]);
    }
}
