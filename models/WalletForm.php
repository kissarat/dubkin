<?php

namespace app\models;


use yii\base\Model;

/**
 * @property User $user
 */
class WalletForm extends Model
{
    public $user;

    public function rules() {
        return [
            ['perfect', 'match', 'pattern' => User::PERFECT]
        ];
    }

    public function attributeLabels()
    {
        return [
            'perfect' => 'Perfect Money'
        ];
    }

    public function attributes()
    {
        $attributes = [];
        foreach($this->user->attributes() as $name) {
            if (0 === strpos($name, 'wallet_')) {
                $attributes[] = str_replace('wallet_', '', $name);
            }
        }
        return $attributes;
    }

    public function __get($name)
    {
        return $this->user->getAttribute('wallet_' . $name);
    }

    public function __set($name, $value)
    {
        $this->user->setAttribute('wallet_' . $name, $value);
    }

    public function save($runValidation = true)
    {
        $attributes = [];
        foreach($this->attributes() as $name) {
            $attributes[] = 'wallet_' . $name;
        }
        return $this->user->save($runValidation, $attributes);
    }
}
