<?php

namespace app\controllers;


use app\models\SettingsForm;
use Yii;
use yii\web\Controller;

class SettingController extends Controller
{
    public function actionIndex($user_id = null) {
        $model = new SettingsForm();
        $model->loadAll($user_id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('app', 'Saved'));
            }
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }
}
