<?php

namespace app\controllers;


use Yii;
use yii\web\Controller;
use yii\web\Response;

class HomeController extends Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionInfo() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'id' => Yii::$app->id,
            'language' => Yii::$app->language,
            'modules' => array_keys(Yii::$app->modules),
            'bootstrap' => Yii::$app->bootstrap,
            'version' => Yii::$app->version,
        ];
    }

    public function actionError() {
        return Yii::$app->getErrorHandler()->exception->getMessage();
    }
}
