<?php

namespace app\controllers;


use app\models\Transfer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class TransferController extends Controller
{
    public function actionIndex($object_id = null) {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Transfer::find()->filterWhere(['object_id' => $object_id]),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ])
        ]);
    }
}
