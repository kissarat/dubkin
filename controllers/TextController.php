<?php

namespace app\controllers;


use app\models\Text;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class TextController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    public function actionEdit()
    {
        $model = $this->findModel();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (false === strpos($model->id, '/')) {
                $this->redirect($this->primaryKey(['view']));
            }
            else {
                $this->redirect('/' . $model->id);
            }
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionView()
    {
        $model = $this->findModel();
        return $this->render('view', [
            'model' => $model,
            'condition' => $_GET,
            'meta' => true
        ]);
    }

    public function findModel() {
        $params = $this->primaryKey();
        $model = Text::findOne($params);
        if (!$model) {
            $model = new Text($params);
            $model->loadFile();
        }
        return $model;
    }

    protected function primaryKey($array = []) {
        if (isset($_GET['id'])) {
            $array['id'] = $_GET['id'];
        }
        $array['lang'] = empty($_GET['lang']) ? Yii::$app->language : $_GET['lang'];
        return $array;
    }
}
