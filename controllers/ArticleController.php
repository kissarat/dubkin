<?php

namespace app\controllers;


use app\models\Article;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class ArticleController extends TextController
{
    public function findModel() {
        $params = $this->primaryKey();
        $model = Article::findOne($params);
        if (!$model) {
            $model = new Article($params);
            $model->loadFile();
        }
        return $model;
    }
}
