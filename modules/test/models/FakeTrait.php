<?php

namespace app\modules\test\models;


use Faker\Generator;

trait FakeTrait
{
    public function fake(Generator $factory) {
        $generated = static::generateData($factory);
        foreach($this->activeAttributes() as $name) {
            if (null === $this->$name && isset($generated[$name])) {
                $this->$name = $generated[$name];
            }
        }
    }
}
