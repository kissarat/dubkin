<?php

namespace app\modules\test\models;


use app\helpers\SQL;
use Faker\Factory;
use yii\base\Model;

class UserGeneratorForm extends Model
{
    public $referral_id;
    public $prefix;
    public $count;

    public function rules()
    {
        return [
            ['count', 'required'],
            [['referral_id', 'prefix'], 'string'],
            ['count', 'integer'],
        ];
    }

    public function generate() {
        $initial = [];
        if (!empty($this->referral_id)) {
            $initial['referral_id'] = $this->referral_id;
        }
        if (empty($this->prefix)) {
            $this->prefix = 'user';
        }
        $prefix_max = SQL::queryCell('SELECT id FROM "user" WHERE id like :id ORDER BY id DESC LIMIT 1', [
           ':id' => $this->prefix . '%'
        ]);
        $number = 1;
        if ($prefix_max && preg_match('/(\d+)$/', $prefix_max, $postfix)) {
            $number = (int) $postfix[1];
        }

        $factory = Factory::create();
        $users = [];
        for($i = 0; $i < $this->count; $i++) {
            $initial['id'] = $this->prefix . ($number + $i);
            $user = new UserFake($initial);
            $user->fake($factory);
            $user->save();
            $users[] = $user;
        }
        return $users;
    }
}
