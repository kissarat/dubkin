<?php

namespace app\modules\test\controllers;


use app\modules\test\models\UserFake;
use Faker\Factory;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class FakeController extends Controller
{
    public function actionUser($locale = Factory::DEFAULT_LOCALE) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return UserFake::generateData(Factory::create($locale));
    }
}
