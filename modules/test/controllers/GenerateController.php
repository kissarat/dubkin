<?php

namespace app\modules\test\controllers;


use app\modules\test\models\UserGeneratorForm;
use Yii;
use yii\web\Controller;

class GenerateController extends Controller
{
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionUser() {
        $model = new UserGeneratorForm();
        $users = [];
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $users = $model->generate();
        }
        return $this->render('user', [
            'model' => $model,
            'users' => $users
        ]);
    }
}
