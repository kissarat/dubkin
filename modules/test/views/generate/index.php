<?php
use yii\helpers\Html;

$items = [
    'Users' => ['user']
];
?>
<div class="generate-index list">
<?php
foreach($items as $label => $url) {
    echo Html::a($label, $url);
}
?>
</div>
