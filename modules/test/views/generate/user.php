<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\modules\test\models\UserGeneratorForm $model */
/** @var \app\models\User[] $users */
?>
<div class="generate-user">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'referral_id') ?>
    <?= $form->field($model, 'prefix') ?>
    <?= $form->field($model, 'count') ?>
    <?= Html::submitButton(Yii::t('app', 'Generate')) ?>

    <?php ActiveForm::end() ?>

    <?php
    $items = [];
    foreach($users as $user) {
        $items[] = implode(' ', [
            Html::a($user->id, ['/user/view', 'id' => $user->id]),
            Html::a($user->referral_id, ['/user/view', 'id' => $user->referral_id]),
        ]);
    }
    echo Html::ul($items);
    ?>
</div>
