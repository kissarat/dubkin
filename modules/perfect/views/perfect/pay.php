<?php
/** @var \app\models\Invoice $model */

use app\models\Config;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="perfect-pay">
<form action="https://perfectmoney.is/api/step1.asp" method="post" data-submit="auto">
    <?= Html::hiddenInput('PAYMENT_ID', $model->id) ?>
    <?= Html::hiddenInput('PAYEE_ACCOUNT', Config::get('perfect', 'wallet')) ?>
    <?= Html::hiddenInput('PAYMENT_AMOUNT', $model->amount) ?>
    <?= Html::hiddenInput('PAYEE_NAME', Yii::$app->name) ?>
    <?= Html::hiddenInput('USER_ID', $model->user_id) ?>
    <?= Html::hiddenInput('PAYMENT_URL', Url::to(['/perfect/perfect/success', 'id' => $model->id], true)) ?>
    <?= Html::hiddenInput('NOPAYMENT_URL', Url::to(['/perfect/perfect/fail', 'id' => $model->id], true)) ?>
    <input name="BAGGAGE_FIELDS" value="USER_ID" type="hidden"/>
    <input name="PAYMENT_UNITS" value="USD" type="hidden"/>
    <input name="PAYMENT_METHOD" type="submit" value="Perfect Money"/>
</form>
</div>
