
CREATE OR REPLACE FUNCTION matrix.descend(_root_id INT)
  RETURNS INT AS $$
DECLARE
  _level INT;
  _id INT;
BEGIN
  SELECT min("level") FROM matrix.degree WHERE root_id = _root_id INTO _level;
  IF _level IS NOT NULL THEN
    SELECT min(id) FROM
      (
        SELECT id FROM matrix.tree WHERE root_id = _root_id AND "level" = _level - 1
        EXCEPT
        SELECT parent_id FROM matrix.tree WHERE root_id = _root_id AND "level" = _level
        GROUP BY parent_id HAVING count(*) >= 2
      ) m
    INTO _id;
  ELSE
    SELECT max("level") FROM matrix.tree WHERE root_id = _root_id INTO _level;
    SELECT min(id) FROM matrix.tree WHERE root_id = _root_id AND "level" = _level INTO _id;
  END IF;
  RETURN _id;
END;
$$ LANGUAGE plpgsql STABLE;


CREATE OR REPLACE FUNCTION matrix.enter(_user_id VARCHAR(24), _parent_id INT)
  RETURNS INT AS $$
BEGIN
  UPDATE "user" u SET account = account + interest FROM matrix.referral r
    JOIN u ON r.user_id = u.id WHERE sponsor_id = _parent_id;
END;
$$ LANGUAGE plpgsql VOLATILE;
