<?php

namespace app\modules\matrix\controllers;


use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class NodeController extends Controller
{
    public function actionIndex($user_id = null) {
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Node::find()->filterWhere(['user_id' => $user_id]),
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionCreate($type_id = null) {
        $model = new Node([
            'type_id' => (int)$type_id,
            'user_id' => Yii::$app->user->id
        ]);
        return $this->edit($model);
    }

    public function edit(Node $model) {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            if ($model->user->account >= $model->type->price
                && $model->log('buy', $model->type->price)
                && $model->open()) {
                $transaction->commit();
                return $this->redirect(['index']);
            }
            else {
                $model->addError('type_id', Yii::t('app', 'Insufficient funds'));
            }
        }

        $accessible = Type::accessible($model->user->account);
        if (empty($accessible)) {
            Yii::$app->session->addFlash('error', Yii::t('app', 'Insufficient funds to open any program'));
        }

        return $this->render('edit', [
            'accessible' => $accessible,
            'model' => $model
        ]);
    }

    public function actionGraph($id, $depth = 12) {
        /** @var Node $model */
        $model = $this->findModel($id);
        return $this->render('graph', [
            'model' => $model,
            'root' => $model->tree($depth),
            'depth' => $depth
        ]);
    }

    protected function findModel($id) {
        $model = Node::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
