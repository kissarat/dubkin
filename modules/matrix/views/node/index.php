<?php
/** @var \yii\data\ActiveDataProvider $dataProvider */
use app\modules\matrix\models\Node;
use app\modules\matrix\models\Type;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<div class="node-index">
    <p>
        <?= Html::a(Yii::t('app', 'Open'), ['create']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'type_id',
                'value' => function(Node $model) {
                    return Type::get($model->type_id)->getName();
                }
            ],
            [
                'attribute' => 'user_id',
                'format' => 'html',
                'value' => function(Node $model) {
                    return Html::a($model->user_id, ['index', 'user_id' => $model->user_id]);
                }
            ],
            'parent_id',
            [
                'label' => Yii::t('app', 'Action'),
                'format' => 'html',
                'value' => function(Node $model) {
                    $items = [
                        Html::a('', ['graph', 'id' => $model->id], [
                            'title' => Yii::t('app', 'Graph'),
                            'class' => 'fa fa-sitemap'
                        ]),
                        Html::a('', ['/transfer/index', 'object_id' => $model->id], [
                            'title' => Yii::t('app', 'Accrue'),
                            'class' => 'fa fa-usd'
                        ]),
                    ];
                    return implode(' ', $items);
                }
            ],
        ]
    ]) ?>
</div>
