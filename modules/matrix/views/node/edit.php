<?php
use app\modules\matrix\models\Node;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $model Node */
/** @var $accessible array */
?>
<div class="node-edit" data-action="<?= Yii::$app->controller->action->id ?>">
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'type_id')->dropDownList($accessible) ?>
<?= Html::submitButton(Yii::t('app', 'Open')) ?>

<?php ActiveForm::end(); ?>
</div>
