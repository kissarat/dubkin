<?php
use yii\helpers\Html;

/** @var \app\modules\matrix\models\Node $model */
/** @var array $root */
/** @var int $depth */
if ($model->parent_id) {
    echo Html::a(Yii::t('app', 'Up'), ['graph', 'id' => $model->parent_id, 'depth' => $depth]);
}
?>
<style>
    .node circle {
        fill: #fff;
        stroke: steelblue;
        stroke-width: 3px;
    }

    .node text { font: 12px sans-serif; }

    .link {
        fill: none;
        stroke: #ccc;
        stroke-width: 2px;
    }
</style>
<article></article>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script src="/graph.js"></script>
<script>
    root = <?= json_encode($root) ?>;
    build_graph(root);
</script>
