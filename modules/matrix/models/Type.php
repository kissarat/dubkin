<?php

namespace app\modules\matrix\models;


use app\helpers\SQL;
use app\models\User;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\NotFoundHttpException;


/**
 * @property integer $id
 * @property number $price
 * @property string $name
 * @property boolean $enabled
 */
class Type extends ActiveRecord
{
    private static $_all;

    public static function tableName()
    {
        return 'matrix.type';
    }

    /**
     * @return static[]
     */
    public static function all()
    {
        if (!static::$_all) {
            static::$_all = Type::find()
                ->orderBy(['id' => SORT_ASC])
                ->indexBy('id')
                ->all();
        }
        return static::$_all;
    }

    /**
     * @param $id
     * @return static
     */
    public static function get($id)
    {
        return static::all()[$id];
    }

    public function getName() {
        return Yii::t('app', 'Program') . ' ' . $this->price;
    }

    /**
     * @return string[]
     */
    public static function enum()
    {
        $list = [];
        foreach(static::all() as $type) {
            $list[$type->id] = $type->getName();
        }
        return $list;
    }

    public static function accessible($amount) {
        $names = [];
        foreach(static::all() as $type) {
            if ($amount >= $type->price) {
                $names[$type->id] = $type->getName();
            }
        }
        return $names;
    }
}
