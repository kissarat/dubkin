<?php

namespace app\modules\matrix\models;


use yii\db\ActiveRecord;

/**
 * @property integer type_id
 * @property string user_name
 * @property string ref_name
 * @property integer level
 * @property number interest
 * @property string root
 * @property string time
 */
class Referral extends ActiveRecord {
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'level' => 'Уровень',
            'user_name' => 'Пользователь',
            'interest' => 'Прибыль',
            'time' => 'Время',
        ];
    }
}
