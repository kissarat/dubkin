<?php

namespace app\modules\matrix\models;


trait UserNodeTrait
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodes() {
        return $this->hasMany(Node::class, ['user_id' => 'id']);
    }
}
