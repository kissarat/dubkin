<?php

namespace app\modules\matrix\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property integer id
 * @property integer number
 * @property integer level
 * @property integer root_id
 * @property string user_id
 * @property number interest
 */
class Tree extends ActiveRecord {
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'number' => Yii::t('app', 'Number'),
            'level' => Yii::t('app', 'Level'),
            'user_id' => Yii::t('app', 'Referral'),
            'interest' => Yii::t('app', 'Income'),
        ];
    }

    /**
     * @param $root_id
     * @param $level
     * @return ActiveQuery
     */
    public static function getLevel($root_id, $level) {
        return static::find()->where([
            'root_id' => $root_id,
            'level' => $level
        ]);
    }
}
