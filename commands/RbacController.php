<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $manage = $auth->createPermission('manage');
        $manage->description = 'Manage site';
        $auth->add($manage);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $manage);

        $auth->assign($admin, 'admin');
    }
}
