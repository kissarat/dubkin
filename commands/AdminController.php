<?php

namespace app\commands;


use app\models\Config;
use app\models\User;
use Yii;
use yii\console\Controller;

class AdminController extends Controller {
    public function actionCreate() {
        umask(0000);
        $list = ['runtime', 'web/assets', 'web/feedback', 'log/perfect/success',
            'web/data/text/ru', 'web/data/text/en'];
        foreach($list as $dir) {
            Yii::$app->local->createDir($dir);
            chmod($dir, 0777);
        }
    }

    public function actionDelete() {
        foreach(['runtime', 'web/assets', 'web/feedback', 'log', 'web/data/text'] as $dir) {
            if (is_dir(Yii::getAlias('@app/' . $dir))) {
                Yii::$app->local->deleteDir($dir);
            }
        }
    }

    public function actionInit() {
        $this->actionDelete();
        $this->actionCreate();
    }

    public function actionUser($id, $password = 1) {
        $user = new User([
            'id' => $id,
            'email' => $id . '@yopmail.com'
        ]);
        $user->setPassword($password);
        if (!$user->save()) {
            echo json_encode($user->getErrors(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\n";
        }
    }

    public function actionPassword($user_id, $password = 1) {
        /** @var User $user */
        $user = User::findOne($user_id);
        $user->setPassword($password);
        $user->save(true, ['hash']);
    }

    public function actionGet($category, $name) {
        echo Config::get($category, $name) . "\n";
    }

    public function actionSet($category, $name, $value) {
        Config::set($category, $name, $value);
    }
}
