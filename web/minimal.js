
function $$(selector) {
    return document.querySelector(selector)
}
function $id(id) {
    return document.getElementById(id)
}
function $all(selector) {
    return document.querySelectorAll(selector)
}
function $class(name) {
    return document.getElementsByClassName(name)
}
function $new(name) {
    return document.createElement(name)
}
function on(target, name, call) {
    if (!call) {
        call = name;
        name = 'click';
    }
    if ('string' == typeof target) {
        target = document.querySelectorAll(target);
        for (var i = 0; i < target.length; i++)
            target[i].addEventListener(name, call);
    }
    else
        target.addEventListener(name, call);
}

function $a(label, url) {
    var a = $new('a');
    a.setAttribute('href', url);
    a.innerHTML = label;
    return a;
}

function $row() {
    var row = $new('tr');
    console.log(arguments);
    for (var i = 0; i < arguments.length; i++) {
        var cell = arguments[i];
        if (cell instanceof HTMLTableCellElement) {
            row.appendChild(cell);
        }
        else if (cell instanceof Element) {
            row.insertCell(-1).appendChild(cell);
        }
        else if ('string' == typeof cell) {
            row.insertCell(-1).innerHTML = cell;
        }
        else {
            row.insertCell(-1).innerHTML = cell.toString();
        }
    }
    return row;
}

function each(array, call) {
    Array.prototype.forEach.call(array, call);
}

function array(a) {
    return Array.prototype.slice.call(a);
}

function $each(selector, call) {
    if ('string' == typeof selector) {
        selector = $all(selector);
    }
    return each(selector, call);
}

function fake(params) {
    var form = document.forms[0];
    var url = '/test/fake/' + form.name.toLowerCase();
    if (params) {
        if ('string' == typeof params) {
            params = {locale: params};
        }
        url += '?' + jQuery.param(params);
    }
    jQuery.getJSON(url, function(data) {
        each(form.elements, function(control) {
            var name = /^\w+\[(\w+)]$/.exec(control.name);
            if (name && !control.value) {
                control.value = data[name[1]];
            }
        });
    });
}
