$('select[placeholder]').each(function(i, select) {
    $('<option />')
        .text(select.getAttribute('placeholder'))
        .attr('disabled', true)
        .attr('selected', true)
        .prependTo(select);
});

$('[name=User]').on('beforeSubmit', function(e) {
    if (agree.checked) {
        return true;
    }
    alert('You must confirm agreement');
    return false;
});

//$('form[data-submit=auto]').submit();
