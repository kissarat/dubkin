<?php

require_once 'pdo.php';

$name = $config['components']['db']['username'];
$password = $config['components']['db']['password'];

$sock = '/var/run/postgresql';
if (!file_exists($sock)) {
    die('PostgreSQL socket file does not exists');
}

pg_connect('host=' . $sock);

try {
    pg_query("DROP DATABASE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

try {
    pg_query("DROP ROLE \"$name\"");
}
catch (Exception $ex) {
    echo $ex->getMessage();
}

pg_query("CREATE ROLE \"$name\" LOGIN PASSWORD '$password'");
pg_query("CREATE DATABASE \"$name\" OWNER \"$name\" CONNECTION LIMIT = 10");

pg_close();
