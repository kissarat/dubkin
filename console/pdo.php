<?php
require_once __DIR__ . '/../config/common.php';

function connect() {
    global $config;
    return new PDO($config['components']['db']['dsn'],
        $config['components']['db']['username'], $config['components']['db']['password'], [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
}
